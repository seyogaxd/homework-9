function isValidAge(value) {
  return value !== null && value !== "" && !isNaN(value);
}

function isValidName(value) {
  return value !== null && value !== "";
}

let userName;
let userAge;

do {
  userName = prompt("Введіть ваше ім'я:");
  userAge = prompt("Введіть ваш вік:");
} while (!isValidAge(userAge) || !isValidName(userName));

userAge = parseInt(userAge);

if (userAge < 18) {
  alert("You are not allowed to visit this website.");
} else if (userAge >= 18 && userAge <= 22) {
  const confirmResult = confirm("Are you sure you want to continue?");
  if (confirmResult) {
    alert(`Welcome, ${userName}`);
  } else {
    alert("You are not allowed to visit this website.");
  }
} else {
  alert(`Welcome, ${userName}`);
}
